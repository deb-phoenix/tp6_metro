// stocke les types de transports
const semitag = "SEM";
const transisere = "C38";
const touGo = "GSV";
const sncf = "SNC";
const paysVoiron = "TPV";

const tram = 'TRAM';
const chrono = 'CHRONO';
const flexo = 'FLEXO';
const proximo = 'PROXIMO';
const scolaires = 'SCOL';
const structurantes = 'Structurantes';
const secondaires = 'Secondaires';
const urbaines = 'Urbaines';
const interurbaines = 'Interurbaines';
const tad = 'TAD';
const c38 = 'C38';
const snc = 'SNC';

// stocke les lignes de transports
const urlLignes = 'https://data.metromobilite.fr/api/routers/default/index/routes';

// Leaflet
const myMap = L.map('myMap').setView([45.191816, 5.721656], 12);

$(document).ready(() => {
// affichage des données de lignes
  function affichageDesLignes(data, transporteur, type) {
    $('#liste-des-lignes').html(''); // vide le container
    $.each(data, (index, value) => {
      if (value.id.match(transporteur)) {
        if (value.type === type) {
        // // crée div + class + id de la ligne
          const boxLigne = $('<div></div>').addClass('boxLigne').attr('id', value.id);

          // crée icone + type de transport
          const typeLigne = $('<div></div>').html(`<p>${value.type}</p>`).addClass('typeLigne');
          const ligneMode = $('<img>').css('margin-right', '6px');
          if (value.mode === 'BUS') {
            ligneMode.attr({ src: 'assets/img/bus.svg', alt: 'bus' });
          } else if (value.mode === 'TRAM') {
            ligneMode.attr({ src: 'assets/img/tram.svg', alt: 'tram' });
          } else if (value.mode === 'RAIL') {
            ligneMode.attr({ src: 'assets/img/train.svg', alt: 'train' });
          }
          $(typeLigne).prepend(ligneMode);

          // crée le nom de la ligne
          const ligneName = $('<div></div>').addClass('ligneName');
          const ligneShortName = $('<p></p>').text(value.shortName).addClass('ligneShortName');
          if (value.color === '') {
            $(ligneShortName).css('background-color', '#ccc'); // si pas de couleur, gris très pale
          } else {
            $(ligneShortName).css({ color: `#${value.textColor}`, 'background-color': `#${value.color}` });
          }
          if (value.type === 'TRAM' || value.type === 'CHRONO') {
            $(ligneShortName).addClass('rond');
          }
          const ligneLongName = $('<p></p>').text(value.longName).addClass('ligneLongName');
          $(ligneName).append(ligneShortName, ligneLongName);

          // crée les boutons
          const boxBoutons = $('<div></div>').addClass('boxBoutons');
          const boutonDetail = $('<a></a>').text('Détails').addClass('boutonDetail').attr({ 'data-id': value.id, 'data-type': value.type });
          const boutonFavori = $('<a></a>').text('Ajouter aux favoris').addClass('addFavori').attr('data-id', value.id);
          $(boxBoutons).append(boutonDetail, boutonFavori);

          $(boxLigne).append(typeLigne, ligneName, boxBoutons);
          $('#liste-des-lignes').append(boxLigne);
        }
      }
    });
  }

  // PANNEAU INFOS COVID //

  function alertCovid() {
    $('#infos-covid').slideDown();
  }
  setTimeout(alertCovid, 2000); // s'affiche 2sec après chargement

  $('#infos-covid__close').click(() => { // fait disparaitre l'alert au clic et réapparaitre après 10 sec
    $('#infos-covid').slideUp();
    setTimeout(alertCovid, 10000);
  });


  // MENU TOGGLE //
  // méthode hide pour cacher le sous menu
  $('.navigation ul.subMenu').hide();

  // clic sur une barre du toggle
  $('.navigation li.toggleSubMenu > a').click(function () {
    // si menu ouvert, le ferme
    if ($(this).next('ul.subMenu:visible').length !== 0) {
      $(this).next('ul.subMenu').slideUp('normal', function () { $(this).parent().removeClass('open'); });
    // si menu fermé, ferme les autres menus et ouvre
    } else {
      $('.navigation ul.subMenu').slideUp('normal', function () { $(this).parent().removeClass('open'); });
      $(this).next('ul.subMenu').slideDown('normal', function () { $(this).parent().addClass('open'); });
    }
  });

  // si clic sur un lien
  $('.navigation ul.subMenu li > a').click(() => {
    // ferme tous les menus
    $('.navigation ul.subMenu').slideUp('normal', function () { $(this).parent().removeClass('open'); });
    // affichage du bon container
    $('#liste-des-lignes').fadeIn();
    $('#ligne-details').fadeOut();
    $('#accueil').hide();
  });



  $.ajax({
    dataType: 'json',
    url: urlLignes,
  }).done((result) => {
    $('#getTram').click(() => {
      affichageDesLignes(result, semitag, tram);
    });
    $('#getChrono').click(() => {
      affichageDesLignes(result, semitag, chrono);
    });
    $('#getProximo').click(() => {
      affichageDesLignes(result, semitag, proximo);
    });
    $('#getFlexo').click(() => {
      affichageDesLignes(result, semitag, flexo);
    });
    $('#getC38').click(() => {
      affichageDesLignes(result, transisere, c38);
    });
    $('#getTransScol').click(() => {
      affichageDesLignes(result, transisere, scolaires);
    });
    $('#getStruct').click(() => {
      affichageDesLignes(result, touGo, structurantes);
    });
    $('#getSecond').click(() => {
      affichageDesLignes(result, touGo, secondaires);
    });
    $('#getTougScol').click(() => {
      affichageDesLignes(result, touGo, scolaires);
    });
    $('#getUrba').click(() => {
      affichageDesLignes(result, paysVoiron, urbaines);
    });
    $('#getTransurb').click(() => {
      affichageDesLignes(result, paysVoiron, interurbaines);
    });
    $('#getTad').click(() => {
      affichageDesLignes(result, paysVoiron, tad);
    });
    $('#getVoirScol').click(() => {
      affichageDesLignes(result, paysVoiron, scolaires);
    });
    $('#getSnc').click(() => {
      affichageDesLignes(result, sncf, snc);
    });
  });

  // FAVORIS //

  // ajout favori
  // enregistre l'identifiant de la ligne dans le localStorage

  function notFav(identifiant) { // si un id est dans le localStorage (return true si non)
    let occurence = 0;
    for (let i = 0; i < localStorage.length; i += 1) {
      // datas du local storage : clef + valeur
      const key = localStorage.key(i);
      const value = localStorage.getItem(key);
      if (value === identifiant) {
        occurence =+ 1;
      }
    }
    if (occurence === 0) {
      return true;
    }
  }

  $('#liste-des-lignes').on('click', '.addFavori', (e) => {
    itemNotInlocalSt = notFav($(e.target).attr('data-id'));
    if (itemNotInlocalSt) {
      const nb = Date.now();
      localStorage.setItem(`favori ${nb}`, $(e.target).attr('data-id'));
      swal({
        title: $(e.target).attr('data-shortname'),
        text: 'Ajouté à vos favoris!',
        icon: "success",
        className: 'sweetalert',
        buttons: false,
        timer: 2000,
      });
    } else {
      swal({
        title: $(e.target).attr('data-shortname'),
        text: 'Déjà dans vos favoris!',
        className: 'sweetalert',
        buttons: false,
        timer: 2000,
      });
    }
  });

  // supprime un favori
  $('#liste-des-lignes').on('click', '.deleteFavori', (e) => {
    localStorage.removeItem($(e.target).attr('data-favkey')); // efface du localStorage
    $(e.target).parents('.boxLigne').remove(); // efface le bloc affiché

    if (localStorage.length === 0) {
      $('#pas-de-favori').fadeIn(); // affiche le message d'alerte 'pas de favoris'
    }
  });

  // tableau pour récupérer les données du localStorage
  let favoris = [];

  function getItemsStorage() {
    for (let i = 0; i < localStorage.length; i += 1) {
      // datas du local storage : clef + valeur
      const key = localStorage.key(i);
      const value = localStorage.getItem(key);

      // créé un objet pour chaque favori
      const favItem = {};
      favItem.favNb = key;
      favItem.ligneId = value;

      // envoie l'objet dans le tableau
      favoris.push(favItem);
    }
  }

  // affichage des données favoris
  function affichageFav(data, key) {
  // création d'une div et ajout d'un id (= id de la ligne) + class
    const boxLigne = $('<div></div>').addClass('boxLigne').attr('id', data[0].id);
    // mode (icone) + type de transport
    const typeLigne = $('<div></div>').html(`<p>${data[0].type}</p>`).addClass('typeLigne');
    const ligneMode = $('<img>').css('margin-right', '6px');
    if (data[0].mode === 'BUS') {
      ligneMode.attr({ src: 'assets/img/bus.svg', alt: 'bus' });
    } else if (data[0].mode === 'TRAM') {
      ligneMode.attr({ src: 'assets/iag/tram.svg', alt: 'tram' });
    } else if (data[0].mode === 'RAIL') {
      ligneMode.attr({ src: 'assets/img/train.svg', alt: 'train' });
    }
    $(typeLigne).prepend(ligneMode);

    // mom de la ligne
    const ligneName = $('<div></div>').addClass('ligneName');
    const ligneShortName = $('<p></p>').text(data[0].shortName).addClass('ligneShortName');
    if (data[0].color === '') {
      $(ligneShortName).css('background-color', 'rgb(228, 228, 228)'); // si pas de couleur definie = gris clair
    } else {
      $(ligneShortName).css({ color: `#${data[0].textColor}`, 'background-color': `#${data[0].color}` });
    }
    if (data[0].type === 'TRAM' || data[0].type === 'CHRONO') {
      $(ligneShortName).addClass('rond');
    }
    const ligneLongName = $('<p></p>').text(data[0].longName).addClass('ligneLongName');
    $(ligneName).append(ligneShortName, ligneLongName);

    // BOUTONS détails + favoris
    const boxBoutons = $('<div></div>').addClass('boxBoutons');
    const boutonDetail = $('<a></a>').text('Détails').addClass('boutonDetail').attr({ 'data-id': data[0].id, 'data-type': data[0].type });
    const boutonFavori = $('<a></a>').text('Supprimer le favori').addClass('deleteFavori').attr('data-favkey', key);
    $(boxBoutons).append(boutonDetail, boutonFavori);

    $(boxLigne).append(typeLigne, ligneName, boxBoutons);
    $('#liste-des-lignes').append(boxLigne);
  }

  // FAVORIS
  $('#fav').on('click', () => {
    $('#ligne-details').fadeOut();
    $('#liste-des-lignes').fadeIn();
    $('#liste-des-lignes').html('');
    $('#accueil').hide(); // vide le container
    const noFav = $('<p></p>').text('Vous n\'avez pas de favori.').attr('id', 'pas-de-favori');
    $('#liste-des-lignes').append(noFav);
    noFav.hide();
    favoris = []; // vide le tableau
    getItemsStorage();
    if (localStorage.length) {
      const urlFavoris = 'https://data.metromobilite.fr/api/routers/default/index/routes?codes='; // + id de la ligne
      $.each(favoris, (index, value) => {
        const idStorage = value.ligneId;
        const keyStorage = value.favNb;
        $.ajax({
          dataType: 'json',
          url: urlFavoris + idStorage,
        }).done((result) => {
          affichageFav(result, keyStorage);
        });
      });
    } else {
      $(noFav).show();
    }
  });

  // supprime un favori
  $('#liste-des-lignes').on('click', '.deleteFavori', (e) => {
    localStorage.removeItem($(e.target).attr('data-favkey')); // efface du localStorage
    $(e.target).parents('.boxLigne').remove(); // efface le bloc affiché

    if (localStorage.length === 0) {
      $('#pas-de-favori').fadeIn();
    }
  });


  // HORAIRES //

  // affichage des horaires si disponibles
  function affichageHoraires(arretsArray) {
    const tableau = $('<table></table>').addClass('tableauHoraires');
    $('#affichage-horaires').append(tableau);
    const caption = $('<caption></caption>');
    const tbody = $('<tbody></tbody>');
    $(tableau).append(caption, tbody);

    // le titre du tableau indique le terminus (dernier item de l'array)
    $(caption).text(`Direction ${arretsArray[arretsArray.length - 1].stopName}`);

    function heures(trip) {
      let monHoraire = '';
      if (trip === '|') {
        monHoraire = '—';
      } else {
        const date = new Date();
        date.setUTCHours(0);
        date.setUTCMinutes(0);
        date.setUTCSeconds(0);
        const d = new Date(date.getTime() + (trip * 1000));
        monHoraire = `${d.getHours()}h${String(d.getMinutes()).padStart(2, '0')}`;
      }
      return monHoraire;
    }

    $.each(arretsArray, (index, value) => {
      const ArretsRow = $('<tr></tr>');
      const ArretsName = $('<td></td>').text(value.stopName);
      const ArretsTime1 = $('<td></td>').css('text-align', 'center').text(heures(arretsArray[index].trips[0]));
      const ArretsTime2 = $('<td></td>').css('text-align', 'center').text(heures(arretsArray[index].trips[1]));
      const ArretsTime3 = $('<td></td>').css('text-align', 'center').text(heures(arretsArray[index].trips[2]));
      const ArretsTime4 = $('<td></td>').css('text-align', 'center').text(heures(arretsArray[index].trips[3]));

      $(ArretsRow).append(ArretsName, ArretsTime1, ArretsTime2, ArretsTime3, ArretsTime4);
      $(tbody).append(ArretsRow);
    });
  }

  // affiche les détails au clic du bouton "afficher les horaires"
  const urlHoraires = 'https://data.metromobilite.fr/api/ficheHoraires/json?route='; // ajouter id de la ligne
  const urlDetails = 'https://data.metromobilite.fr/api/lines/json?types=ligne&codes='; // ajouter id de la ligne
  $('#liste-des-lignes').on('click', '.boutonDetail', (e) => {
    const identifiantLigne = $(e.target).attr('data-id');
    $('#liste-des-lignes').fadeOut();
    $('#ligne-details').fadeIn();
    $('#affichage-horaires').empty();
    $('#pas-de-data').hide();

    $.ajax({
      dataType: 'json',
      url: urlDetails + identifiantLigne,
    }).done((result) => {
    // affichage du nom de la ligne en titre (+ numéro & couleur)
      const details = result.features[0].properties;
      $('#ligne-details__icone').text(details.NUMERO).css({ 'background-color': `rgb(${details.COULEUR})`, color: `rgb(${details.COULEUR_TEXTE})` });
      const type = $(e.target).attr('data-type');
      if (type === 'TRAM' || type === 'CHRONO') {
        $('#ligne-details__icone').addClass('round');
      } else {
        $('#ligne-details__icone').removeClass('round');
      }
      $('#ligne-details__nom').text(details.LIBELLE);

      // LEAFLET //
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(myMap);
      const geoJson = result; //  à remplir
      const myColor = `rgb(${details.COULEUR})`; // à remplir
      L.geoJSON(geoJson, {
        style: {
          color: myColor,
        },
      }).addTo(myMap);
    });
    $.ajax({
      dataType: 'json',
      url: urlHoraires + identifiantLigne,
    }).done((result) => {
      if (result[0].arrets.length === 0) {
        $('#pas-de-data').show();
      } else {
        $('#pas-de-data').hide();
        affichageHoraires(result[0].arrets);
      }

      if (result[1].arrets.length === 0) {
        $('#pas-de-data').show();
      } else {
        $('#pas-de-data').hide();
        affichageHoraires(result[1].arrets);
      }
    });
  });



});
