<p align="center">
<img width="20%" src="assets/img/logo-metromobilite.png">
</p>

# WELCOME
MyMetromobilité, toute la mobilité sur l'aire Grenobloise!
<br/>

## BRIEF
Créer un site internet responsive dont l’objectif est de pouvoir consulter les horaires de toutes les lignes des transports en commun de l’agglomération Grenobloise.
<br/>

## INSTALLATION
Pour utiliser le projet, installer les dépendances avec npm (<code>npm i jquery</code> et <code>npm i leaflet</code>), ouvrir index.html dans le navigateur.
<br/>

## TECHNO-TAGS
- HTML5
- CSS3
- SASS
- JS ES6+
- Json
- NPM
- Jquery/ Ajax
- Leaflet/ Geojson
- SweetAlert
- Recettage

## AUTEURE
- Déb Phoenix ~ *good vibes only*
- Proudly student powered by: http://simplon.co

## COPYRIGHT
- [Metromobilité M](https://www.mobilites-m.fr)
- [Leaflet](https://leafletjs.com/)